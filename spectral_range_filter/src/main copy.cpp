/* ----------------------------------------------------------------------------

 * GTSAM Copyright 2010, Georgia Tech Research Corporation,
 * Atlanta, Georgia 30332-0415
 * All Rights Reserved
 * Authors: Frank Dellaert, et al. (see THANKS for the full author list)

 * See LICENSE for the license information

 * -------------------------------------------------------------------------- */

/**
 * @file Pose2SLAMExampleExpressions.cpp
 * @brief Expressions version of Pose2SLAMExample.cpp
 * @date Oct 2, 2014
 * @author Frank Dellaert
 */

#include "Eigen/Core"
#include "Eigen/Geometry"
// The two new headers that allow using our Automatic Differentiation Expression framework
#include <gtsam/slam/expressions.h>
#include <gtsam/nonlinear/ExpressionFactorGraph.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>

#include <gtsam/geometry/Pose2.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/geometry/Point3.h>
#include <gtsam/nonlinear/Marginals.h>
#include <gtsam/inference/Symbol.h>

// The two new headers that allow using our Automatic Differentiation Expression framework
#include <math.h>       /* log */

using namespace std;
using namespace gtsam;

#include "2d3drangefactor.h"

double rrc_2d3dRangeFactor(const gtsam::Pose2& pose, const gtsam::Point3& point, OptionalJacobian<1, 3> H1 = boost::none, OptionalJacobian<1, 3> H2 = boost::none) {


  double r = sqrt((pose.x() - point.x()) * (pose.x() - point.x()) + (pose.y() - point.y()) * (pose.y() - point.y()) + point.z() * point.z());
  if (H1) (*H1) = (Matrix(1,3) << -(point.x() - pose.x())/r, 
                                  -(point.y() - pose.y())/r, 
                                  0).finished();
  if (H2) (*H2) = (Matrix(1,3) << (point.x() - pose.x())/r, 
                                  (point.y() - pose.y())/r, 
                                  point.z()/r).finished();
  return r;
};

inline Expression<double> rrc_2d3dRangeFactor_(const gtsam::Pose2_& base_pose, const gtsam::Point3_& point, const gtsam::Pose2_& sensor_tf) {
    Expression<Pose2> body_T_sensor__(sensor_tf);
    Expression<Pose2> nav_T_body_(base_pose);
    Expression<Pose2> nav_T_sensor_(traits<Pose2>::Compose, nav_T_body_,
                                 body_T_sensor__);

    return Expression<double>(&rrc_2d3dRangeFactor, nav_T_sensor_, point);
};


int main(int argc, char** argv) {

    // 1. Create a factor graph container and add factors to it
  ExpressionFactorGraph graph;

  // // Create Expressions for unknowns
  Pose2_ x1('x',1), x2('x',2), x3('x',3), x4('x',4), x5('x',5);
  Point3_ l1('l',1);
  //
  // // 2a. Add a prior on the first pose, setting it to the origin
  noiseModel::Diagonal::shared_ptr priorNoise = noiseModel::Diagonal::Variances((Vector(3) << 1e-2, 1e-2, 1e-2).finished());
  graph.addExpressionFactor(x1, (Pose2(0, 0, 0)), priorNoise);
  // For simplicity, we will use the same noise model for odometry and loop closures
  noiseModel::Diagonal::shared_ptr model = noiseModel::Diagonal::Variances((Vector(3) << 1e-2, 1e-2, 1e-2).finished());
  // Range model
  noiseModel::Base::shared_ptr gaussian; // non-robust
  noiseModel::Base::shared_ptr tukey;
  noiseModel::Base::shared_ptr rangeModel;  
  bool robust       = true;
  double sigmaR     = 0.05;                               // range standard deviation
  rangeModel  = noiseModel::Isotropic::Sigma(1, sigmaR);  // non-robust
  // tukey       = noiseModel::Robust::Create(noiseModel::mEstimator::Huber::Create(1), gaussian);
  // rangeModel  = robust ? tukey : gaussian;

  Pose2 sensor_tf = Pose2(0,0,0);

  // 2b. Add odometry factors
  graph.addExpressionFactor(between(x1,x2), (Pose2(2.0, 0, 0     )), model);
  graph.addExpressionFactor(between(x2,x3), (Pose2(2.0, 0, M_PI_2)), model);
  graph.addExpressionFactor(between(x3,x4), (Pose2(2.0, 0, M_PI_2)), model);
  graph.addExpressionFactor(between(x4,x5), (Pose2(2.0, 0, M_PI_2)), model);

  graph.addExpressionFactor(rrc_2d3dRangeFactor_(Pose2_('x',1), l1, sensor_tf), 3.3166*1.00, rangeModel);
  graph.addExpressionFactor(rrc_2d3dRangeFactor_(Pose2_('x',2), l1, sensor_tf), 1.7321, rangeModel);
  graph.addExpressionFactor(rrc_2d3dRangeFactor_(Pose2_('x',3), l1, sensor_tf), 1.7321, rangeModel);
  graph.addExpressionFactor(rrc_2d3dRangeFactor_(Pose2_('x',4), l1, sensor_tf), 1.7321, rangeModel);
  graph.addExpressionFactor(rrc_2d3dRangeFactor_(Pose2_('x',5), l1, sensor_tf), 1.7321, rangeModel);

  Values initialEstimate;
  initialEstimate.insert(Symbol('x',1), Pose2(0.5, 0.0,  0.2   ));
  initialEstimate.insert(Symbol('x',2), Pose2(2.3, 0.1, -0.2   ));
  initialEstimate.insert(Symbol('x',3), Pose2(4.1, 0.1,  M_PI_2));
  initialEstimate.insert(Symbol('x',4), Pose2(4.0, 2.0,  M_PI  ));
  initialEstimate.insert(Symbol('x',5), Pose2(2.1, 2.1, -M_PI_2));
  initialEstimate.insert(Symbol('l',1), Point3(5.3,1.3,1.3));

  // 4. Optimize the initial values using a Gauss-Newton nonlinear optimizer
  GaussNewtonParams parameters;
  parameters.relativeErrorTol = 1e-5;
  parameters.maxIterations = 100;
  GaussNewtonOptimizer optimizer(graph, initialEstimate, parameters);
  Values result = optimizer.optimize();
  result.print("Final Result:\n");

  return 0;
}
