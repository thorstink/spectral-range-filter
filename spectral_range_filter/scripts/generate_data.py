import csv, matplotlib.pyplot as plt, numpy as np
from mpl_toolkits.mplot3d import Axes3D

mu_base, sigma_base = 0, 0.1 # mean and standard deviation


with open('data/filtered_scans.csv') as csvfile:
	reader = csv.reader(csvfile, delimiter=',', quotechar="|")
	data = list(reader)
	row_count = len(data)

	PoseRangeMatrix = np.empty((0, 5))
	LandMarkMatrix 	= np.array([[-12.5,-2,0.5],[-4,3,0.55],[5,0,0.45]])

	print("{} and {}".format("string", row_count))
	i = 0
	for row in data:
		result = map(float, np.append(row[0:4],1.0))
		PoseRangeMatrix = np.append(PoseRangeMatrix, [result], axis=0)
		
		# Create a fake measurement to a random beacon.
		# 1. get a beacon to measure with
		beacon_id = np.random.random_integers(0, high=LandMarkMatrix.shape[0]-1)
		# 2. create a perfect measurement, the eucledian distance between pose and point
		pose3d = np.append(result[1:3], [0], axis=0)
		beacon_loc = LandMarkMatrix[beacon_id,:]
		range = np.linalg.norm(pose3d-beacon_loc)
		# 3. Add noise
		s_base = np.random.normal(mu_base, sigma_base, 1)
		range += s_base
		print("Original: {}, corrupt: {}".format(range-s_base, range))

		# write out to CSV.


	# fig = plt.figure()
	# ax 	= fig.add_subplot(111, projection='3d')
	# for row in LandMarkMatrix:
	# 	ax.scatter(row[1], row[2], row[3], c='r', marker='o', label='landmarks')
	# ax.plot(PoseRangeMatrix[:,1], PoseRangeMatrix[:,2], 0.0, label='robot path')
	# ax.set_aspect('equal')
	# plt.show()

	s_base = np.random.normal(mu_base, sigma_base, 1)
	# print("Noise: {}".format(s_base))
	# make measurements
	a = np.random.random_integers(1, high=3)
	print(a)




